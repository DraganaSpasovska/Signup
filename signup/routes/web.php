<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/')->name('home')->uses('UserController@form');
Route::post('/user')->name('user')->uses('UserController@saveUser');
Route::get('/user/{email}/confirmation/{code}')->name('confirmation')->uses('UserController@confirmation');
Route::get('/user/profile/{id}')->name('profile')->uses('UserController@profile');