<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request; 
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class UserController extends Controller
{
    public function form()
    {
    	return view('signup-form');
    }

    public function saveUser(Request $request)
    {
    	$data = $request->all();
    	$name = $data['name'];
    	$email = $data['email'];
    	$password = $data['password'];

    	$user = new User;
    	$user->name = $name;
    	$user->email = $email;
    	$user->password = $password;
    	$user->save();

    	$faker = Faker::create();
    	$code = $faker->ean8;

    	return redirect()->route('confirmation', ['email' => $email, 'code' => $code]);
    }

    public function confirmation($email, $code)
    {
    	Log::info("http://127.0.0.1:8000/user/$email/confirmation/$code");

    	User::where('email', $email)->update(['is_active' => 1,'code' => $code]);
    	$user = User::where('email', $email)->get();
    	foreach ($user as $object) {
    		$id = $object->id;
    	}
    	return redirect()->route('profile', ['id' => $id]);
    }

    public function profile($id)
    {
    	$user = User::where('id', $id)->get();
    	foreach ($user as $object) {
    		$name = $object->name;
    	}
    	return view('profile', ['name' => $name]);
    }
}
